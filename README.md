# Проверочное задание

## Задача

Текст задания приведен ниже. Выполненные части помечаются зачеркиванием текста.

~~Попробуйте у себя дома установить python~~, ~~bottle~~, ~~mongodb~~, ~~и сделать что-нибудь простое, например, в БД создать коллекцию - список книг с полями - [автор, дата выхода книги, жанр, пометка]~~ и из python попробуйте подобавлять и поудалять записи в коллекции. Для работы с монго можно использовать редактор - robomongo или mongochief. ~~Будет замечательно если на бэкбоне напишите вывод этого списка книг~~ и добавление новой книги, верстка не важна, можете использовать бустрап или просто таблицы.

## Выполненные действия
1. Установлено и настроено необходимое ПО
1. Сгенерированы данные базы
1. Создана и заполнена база
1. Страница стилизована
1. Книги выведены с помощью Backbone

## Необходимое ПО
- Python
- Mongodb
- Bottle: Python Web Framework

## Todo
- Удалить books.json из app
- Запуск всех скриптов из одного места
- Создавать папки "db" и "logs" скриптом
- Запускать "mongod" как службу
- Пагинация или ленивая загрузка
- Загружать только нужные части Material Design Light
- Проверить отображение для разных вьюпортов
- Разбить app.js на отдельные файлы и шаблоны
- Собирать библиотеки и кастомные скрипты в одни файлы, используя RequireJS или Webpack
- После отделения шаблонов перенести корень Backbone app выше по дереву
- Обойтись без jQuery если Backbone позволяет
- Удалить пример коллекции а "app"