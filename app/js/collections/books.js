'use strict';

var app = app || {};

var BookList = Backbone.Collection.extend({
  model: app.Book,
  url: "/books/books.json"
});

app.Books = new BookList();
