'use strict';

Backbone.Model.prototype.idAttribute = '_id';

var app = app || {};

app.Book = Backbone.Model.extend({
  url: "/books",
  defaults: {
    "author": '',
    "release_date": '',
    "genre": '',
    "label": '',
  }
});
