'use strict';

var app = app || {};

app.BookView = Backbone.View.extend({
  model: new app.Book(),
  tagName: 'tr',
  template: _.template($('.book-template').html()),

  // Initialize the app
  initialize: function() {
    this.render();
  },

  // Render the app
  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  }
});
