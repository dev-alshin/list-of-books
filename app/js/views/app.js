'use strict';

var app = app || {};

app.AppView = Backbone.View.extend({
  model: app.Books,
  el: $('#app'),

  initialize: function() {
    this.model.on('add', this.render, this);
    this.model.fetch({
      success: function(response) {
        _.each(response.toJSON(), function(item) {
          console.log('Successfully GOT book with _id: ' + item._id);
        });
      },
      error: function() {
        console.log('Failed to get books');
      }
    });
  },

  render: function() {
    var self = this;
    this.$el.html('');
    _.each(this.model.toArray(), function(book) {
      self.$el.append(new app.BookView({model: book}).render().$el);
    });
    return this;    
  }
});
